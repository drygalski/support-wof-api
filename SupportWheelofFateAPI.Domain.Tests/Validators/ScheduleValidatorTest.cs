﻿using System;
using System.Collections.Generic;
using System.Linq;
using SupportWheelofFateAPI.Domain.Models;
using SupportWheelofFateAPI.Domain.Validators;
using Xunit;

namespace SupportWheelofFateAPI.Domain.Tests.Validators
{
    public class ScheduleValidatorTest
    {
	    private readonly List<SupportDayModel> _correctSchedule;
	    private readonly List<EngineerModel> _allEngineers;
	    
		private EngineerModel TestEngineer1 => new EngineerModel { Id = new Guid("0add76ae-c130-47c3-8b27-2d7a22e53dc1"), Name = "TestEngineer1" };
		private EngineerModel TestEngineer2 => new EngineerModel { Id = new Guid("11571a2f-3746-41f0-8e50-5ca2e0503cbf"), Name = "TestEngineer2" };
		private EngineerModel TestEngineer3 => new EngineerModel { Id = new Guid("ac15e262-9678-4713-b23b-b67482525806"), Name = "TestEngineer3" };
		private EngineerModel TestEngineer4 => new EngineerModel { Id = new Guid("ab4a20a8-6ab5-4791-ae1d-59d725fb83d6"), Name = "TestEngineer4" };
		private EngineerModel TestEngineer5 => new EngineerModel { Id = new Guid("c7c17340-2713-48dd-a963-e0daee2c2d4d"), Name = "TestEngineer5" };
		private EngineerModel TestEngineer6 => new EngineerModel { Id = new Guid("84d060be-50a6-46a3-a378-345dba0bffe3"), Name = "TestEngineer6" };
		private EngineerModel TestEngineer7 => new EngineerModel { Id = new Guid("541ceb99-9a79-4bdd-b99f-7b08a53d0359"), Name = "TestEngineer7" };
		private EngineerModel TestEngineer8 => new EngineerModel { Id = new Guid("2f15c2d1-4229-4ee5-9f48-71dc726dd2d6"), Name = "TestEngineer8" };
		private EngineerModel TestEngineer9 => new EngineerModel { Id = new Guid("ac036028-9b7a-4027-a057-29c2fe38618a"), Name = "TestEngineer9" };
		private EngineerModel TestEngineer10 => new EngineerModel { Id = new Guid("69716563-c4e4-4a8a-8471-a115e86d2616"), Name = "TestEngineer10" };

		public ScheduleValidatorTest()
		{

			_correctSchedule = new List<SupportDayModel>
			{
				new SupportDayModel { CalendarDay = new DateTime(2018, 7, 2) , AmAssignment = TestEngineer1, PmAssignment = TestEngineer3},
				new SupportDayModel { CalendarDay = new DateTime(2018, 7, 3) , AmAssignment = TestEngineer2, PmAssignment = TestEngineer4},
				new SupportDayModel { CalendarDay = new DateTime(2018, 7, 4) , AmAssignment = TestEngineer3, PmAssignment = TestEngineer5},
				new SupportDayModel { CalendarDay = new DateTime(2018, 7, 5) , AmAssignment = TestEngineer4, PmAssignment = TestEngineer6},
				new SupportDayModel { CalendarDay = new DateTime(2018, 7, 6) , AmAssignment = TestEngineer5, PmAssignment = TestEngineer7},
				new SupportDayModel { CalendarDay = new DateTime(2018, 7, 9) , AmAssignment = TestEngineer6, PmAssignment = TestEngineer8},
				new SupportDayModel { CalendarDay = new DateTime(2018, 7, 10) , AmAssignment = TestEngineer7, PmAssignment = TestEngineer9},
				new SupportDayModel { CalendarDay = new DateTime(2018, 7, 11) , AmAssignment = TestEngineer8, PmAssignment = TestEngineer10},
				new SupportDayModel { CalendarDay = new DateTime(2018, 7, 12) , AmAssignment = TestEngineer9, PmAssignment = TestEngineer1},
				new SupportDayModel { CalendarDay = new DateTime(2018, 7, 13) , AmAssignment = TestEngineer10, PmAssignment = TestEngineer2},
			};

			_allEngineers = new List<EngineerModel>
			{
				TestEngineer1,
				TestEngineer2,
				TestEngineer3,
				TestEngineer4,
				TestEngineer5,
				TestEngineer6,
				TestEngineer7,
				TestEngineer8,
				TestEngineer9,
				TestEngineer10
			};
		}

		[Fact]
        public void ValidateSchedule_NullSchedule_Invalid()
        {
			var validator = new ScheduleValidator();
	        var answer = validator.ValidateSchedule(null, null);
			Assert.False(answer);
        }

	    [Fact]
	    public void ValidateSchedule_EmptySchedule_Invalid()
	    {
		    var validator = new ScheduleValidator();
		    var answer = validator.ValidateSchedule(new List<SupportDayModel>(), null);
		    Assert.False(answer);
	    }

	    [Fact]
	    public void ValidateSchedule_AmUnassigned_Invalid()
	    {
		    var schedule = _correctSchedule.ToList();
		    schedule[3].AmAssignment = null;
		    var validator = new ScheduleValidator();
		    var answer = validator.ValidateSchedule(schedule, _allEngineers);
		    Assert.False(answer);
	    }

	    [Fact]
	    public void ValidateSchedule_PmUnassigned_Invalid()
	    {
		    var schedule = _correctSchedule.ToList();
		    schedule[4].PmAssignment = null;
		    var validator = new ScheduleValidator();
		    var answer = validator.ValidateSchedule(schedule, _allEngineers);
		    Assert.False(answer);
	    }

	    [Fact]
	    public void ValidateSchedule_SameDayAssignement_Invalid()
	    {
		    var schedule = _correctSchedule.ToList();
		    schedule[5].AmAssignment = TestEngineer5;
		    schedule[5].PmAssignment = TestEngineer5;
			var validator = new ScheduleValidator();
		    var answer = validator.ValidateSchedule(schedule, _allEngineers);
		    Assert.False(answer);
	    }

	    [Fact]
	    public void ValidateSchedule_DayInRowAssignement_Invalid()
	    {
		    var schedule = _correctSchedule.Take(9).ToList();
		    schedule[5].AmAssignment = TestEngineer5;
		    schedule[6].PmAssignment = TestEngineer5;
		    var validator = new ScheduleValidator();
		    var answer = validator.ValidateSchedule(schedule, _allEngineers);
		    Assert.False(answer);
	    }

		[Fact]
	    public void ValidateSchedule_CorrectLong_Valid()
	    {
		    var validator = new ScheduleValidator();
		    var answer = validator.ValidateSchedule(_correctSchedule, _allEngineers);
		    Assert.True(answer);
	    }

	    [Fact]
	    public void ValidateSchedule_LongEngineerNotAssigned_Valid()
	    {
		    var schedule = _correctSchedule.ToList();
		    schedule[9].AmAssignment = TestEngineer5;
			var validator = new ScheduleValidator();
		    var answer = validator.ValidateSchedule(schedule, _allEngineers);
		    Assert.False(answer);
	    }

		[Fact]
	    public void ValidateSchedule_CorrectShort_Valid()
	    {
		    var validator = new ScheduleValidator();
		    
		    var answer = validator.ValidateSchedule(_correctSchedule.Take(5).ToList(), _allEngineers);
		    Assert.True(answer);
	    }
	}
}
