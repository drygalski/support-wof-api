using System;
using System.Linq;
using SupportWheelofFateAPI.Domain.Services;
using Xunit;

namespace SupportWheelofFateAPI.Domain.Tests.Services
{
	public class EngineersServiceTest
	{
		private readonly IEngineersService _tEngineersManager;

		public EngineersServiceTest()
		{
			_tEngineersManager = new EngineersService();
		}

		[Fact]
		public void GetEngineers_Returns_10()
		{
			const int expected = 10;
			var engineers = _tEngineersManager.GetEngineers();
			Assert.NotEmpty(engineers);
			Assert.Equal(expected, engineers.Count);
		}

		[Fact]
		public void GetEngineers_Returns_ContainsExpectedEngineer()
		{
			var expected = new Guid("4a089dcb-86ab-4b5d-bf5a-fc138b2e7f24");
			var engineers = _tEngineersManager.GetEngineers();
			Assert.Contains(engineers, e => e.Id == expected);
		}

		[Fact]
		public void GetEngineers_AllHaveGuidID()
		{
			var engineers = _tEngineersManager.GetEngineers();
			Assert.All(engineers, e => Assert.False(e.Id == Guid.Empty));
		}

		[Fact]
		public void GetEngineers_AllIdsAreUnique()
		{
			var engineers = _tEngineersManager.GetEngineers();
			var ids = engineers.Select(e => e.Id).Distinct();
			Assert.Equal(engineers.Count, ids.Count());
		}

		[Fact]
		public void GetEngineers_AllHaveNotEmptyNames()
		{
			var engineers = _tEngineersManager.GetEngineers();
			Assert.All(engineers, e => Assert.False(String.IsNullOrEmpty(e.Name)));
		}

		[Fact]
		public void GetEngineers_AllNamesAreUnique()
		{
			var engineers = _tEngineersManager.GetEngineers();
			var names = engineers.Select(e => e.Name).Distinct();
			Assert.Equal(engineers.Count, names.Count());
		}
	}
}