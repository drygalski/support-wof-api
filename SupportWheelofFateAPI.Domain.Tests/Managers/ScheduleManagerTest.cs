using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using SupportWheelofFateAPI.Domain.Managers;
using SupportWheelofFateAPI.Domain.Models;
using SupportWheelofFateAPI.Domain.Services;
using SupportWheelofFateAPI.Domain.Validators;
using Xunit;

namespace SupportWheelofFateAPI.Domain.Tests.Managers
{
    public class ScheduleManagerTest
    {
	    private readonly Mock<IEngineersService> _mEngineersService;
		private readonly Mock<IScheduleValidator> _mScheduleValidator;
	    private readonly Mock<IEngineersQueueManager> _mQueueManager;

		private IList<EngineerModel> _oExampleEngineers
		{
			get
			{
				var examples = new List<string[]>
				{
					new[] {"1ef1d288-eca9-403a-bb1e-96fe21e13052", "John"},
					new[] {"ffd468bf-1c95-4252-b723-77e8e54fa819", "Ola"},
					new[] {"af29599c-ae62-4e25-9df3-7a34d5caa710", "Adam"},
					new[] {"1447ca7c-39b2-464f-91eb-e55f7566cbd9", "Ada"},
					new[] {"4a089dcb-86ab-4b5d-bf5a-fc138b2e7f24", "Konrad"},
					new[] {"1d4bd702-d7a2-465a-86d1-af59a6be1b98", "Lily",},
					new[] {"b5725820-add5-4649-bd82-04f64d08c77c", "Olaf",},
					new[] {"43db7613-9e2d-4cca-b09a-3be97e3cc6fb", "Jeremi"},
					new[] {"f35e2004-8642-40c5-9519-8e1858b8eb69", "Karina"},
					new[] {"2fe3525b-6154-42e2-898f-f7381223403f", "Ash"},
				};
				return examples.Select(e => new EngineerModel { Id = new Guid(e[0]), Name = e[1] }).ToList();
			}
		}

		public ScheduleManagerTest()
		{
			_mEngineersService = new Mock<IEngineersService>();
			_mEngineersService.Setup(m => m.GetEngineers()).Returns(_oExampleEngineers);
			_mScheduleValidator = new Mock<IScheduleValidator>();
			_mScheduleValidator.Setup(v => v.ValidateSchedule(It.IsAny<List<SupportDayModel>>(), It.IsAny<List<EngineerModel>>())).Returns(true);
			_mQueueManager = new Mock<IEngineersQueueManager>();
			_mQueueManager.Setup(m => m.NextAm()).Returns(new EngineerModel {Id = Guid.NewGuid()});
			_mQueueManager.Setup(m => m.NextPm()).Returns(new EngineerModel { Id = Guid.NewGuid() });
		}

		[Fact]
		public void PopulatePlan_NoEngineers_CreatesNullSchedule()
		{
			var manager = new Mock<IEngineersService>();
			manager.Setup(m => m.GetEngineers()).Returns(new List<EngineerModel>());

			var scheduler = new ScheduleManager(manager.Object,_mScheduleValidator.Object, _mQueueManager.Object);
			var schedule = scheduler.GenerateMonthPlan(null, 0, 0);
			Assert.Null(schedule);
		}

		[Fact]
        public void PopulatePlan_StartWithEmpty_CreatesNullSchedule()
        {
	        var scheduler = new ScheduleManager(_mEngineersService.Object,_mScheduleValidator.Object, _mQueueManager.Object);
	        var schedule = scheduler.GenerateMonthPlan(null, 0, 0);
			Assert.Null(schedule);
		}

	    [Fact]
	    public void PopulatePlan_PopulateDefaultDaysAhead_ExpectedScheduleLength()
	    {
		    var expectedDays = 22;
			var scheduler = new ScheduleManager(_mEngineersService.Object,_mScheduleValidator.Object, _mQueueManager.Object);
		    var schedule = scheduler.GenerateMonthPlan(null, 2018, 7);
			Assert.Equal(expectedDays, schedule.ScheduledDayModels.Count);
	    }

		[Fact]
		public void PopulatePlan_StartExactDate_CorrectId()
		{
			const string expectedId = "2018.7";

			var scheduler = new ScheduleManager(_mEngineersService.Object,_mScheduleValidator.Object, _mQueueManager.Object);

			var schedule = scheduler.GenerateMonthPlan(null, 2018, 7);
			Assert.Equal(expectedId, schedule.Id);
		}

		[Fact]
	    public void PopulatePlan_AppendDaysPopulateDefaultDaysAhead_ExpectedCorrectLastDate()
	    {
		    var endDate = new DateTime(2018, 7, 31); //Tuesday

		    var scheduler = new ScheduleManager(_mEngineersService.Object,_mScheduleValidator.Object, _mQueueManager.Object);
		    var schedule = scheduler.GenerateMonthPlan(null, 2018, 7);
			Assert.Equal(endDate, schedule.ScheduledDayModels.Last().CalendarDay);
	    }

	    [Fact]
	    public void PopulatePlan_DefaultEmpty_AllDaysBooked()
	    {
			var scheduler = new ScheduleManager(_mEngineersService.Object,_mScheduleValidator.Object, _mQueueManager.Object);
		    var schedule = scheduler.GenerateMonthPlan(null, 2018, 7);
			Assert.All(schedule.ScheduledDayModels, model => Assert.True(model.IsBooked));
	    }

		[Fact]
		public void PopulatePlan_NotValidated_ReturnsNull()
		{
			var validator = new Mock<IScheduleValidator>();
			validator.Setup(v => v.ValidateSchedule(It.IsAny<List<SupportDayModel>>(), It.IsAny<List<EngineerModel>>())).Returns(false);

			var scheduler = new ScheduleManager(_mEngineersService.Object, validator.Object, _mQueueManager.Object);
			var schedule = scheduler.GenerateMonthPlan(null, 2018, 7);
			Assert.Null(schedule);
		}

		[Fact]
		public void IntegrationTest_CorrectDetail_ExpectedSchedule()
		{
			var scheduler = new ScheduleManager(_mEngineersService.Object, _mScheduleValidator.Object, _mQueueManager.Object);
			var schedule = scheduler.GenerateMonthPlan(null, 2018, 8);
			Assert.NotNull(schedule);
			Assert.Equal(23, schedule.ScheduledDayModels.Count);
			Assert.Equal(new DateTime(2018, 8, 31), schedule.ScheduledDayModels.Last().CalendarDay);

		}
	}
}
