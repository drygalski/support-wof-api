﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using SupportWheelofFateAPI.Domain.Managers;
using SupportWheelofFateAPI.Domain.Models;
using SupportWheelofFateAPI.Domain.Repositories;
using Xunit;

namespace SupportWheelofFateAPI.Domain.Tests.Managers
{
    public class AllocationSwapManagerTest
    {
	    private Mock<IScheduleRepository> _mScheduleRepository;
	    private AllocationSwapManager _tAllocationManager;

	    private EngineerModel _eng0 = new EngineerModel { Id = new Guid("b2ad25eb-0243-458e-a38c-e65e5bac64a6"), Name = "eng0" };
	    private EngineerModel _eng1 = new EngineerModel { Id = new Guid("6d3a82c0-5d8a-4432-8b54-f288fd73a336"), Name = "eng1" };
	    private EngineerModel _eng2 = new EngineerModel { Id = new Guid("7b8d71b8-7020-41a4-a44c-ca2f8d8dbf3f"), Name = "eng2" };
	    private EngineerModel _eng3 = new EngineerModel { Id = new Guid("bf432bda-de53-4d53-993a-83cc3eaa9b9d"), Name = "eng3" };
	    private EngineerModel _eng4 = new EngineerModel { Id = new Guid("7288ce72-850d-4cd2-813a-7646e9fad346"), Name = "eng4" };
	    private EngineerModel _eng5 = new EngineerModel { Id = new Guid("96cef3cb-7bea-4d72-9234-1e96b7237a3b"), Name = "eng5" };
	    private EngineerModel _eng6 = new EngineerModel { Id = new Guid("7359f42b-387a-4b19-8e11-cd42bb739360"), Name = "eng6" };
	    private EngineerModel _eng7 = new EngineerModel { Id = new Guid("becb3140-4cc4-41c6-9be4-4f2c18068b2b"), Name = "eng7" };
	    private EngineerModel _eng8 = new EngineerModel { Id = new Guid("6933f845-cea8-4c3c-9e8e-d6f7b0133b2b"), Name = "eng8" };
	    private EngineerModel _eng9 = new EngineerModel { Id = new Guid("e1f2757c-cc07-4988-a272-b1120cb1137c"), Name = "eng9" };

		public AllocationSwapManagerTest()
	    {
		    _mScheduleRepository = new Mock<IScheduleRepository>();  
			_tAllocationManager = new AllocationSwapManager(_mScheduleRepository.Object);
		}

	    [Fact]
        public async Task GetSwapAllocations_NoSchedule_ReturnsNull()
	    {
		    var scheduleRepository = new Mock<IScheduleRepository>();
		    var allocationManager = new AllocationSwapManager(scheduleRepository.Object);
			var response = await allocationManager.GetSwapAllocations(Guid.NewGuid(), DateTime.Now);
			Assert.Null(response);
	    }

	    [Fact]
	    public async Task GetSwapAllocations_CallsRepositoryExists()
	    {
		    var expected1 = "2018.8";
		    var expected2 = "2018.9";
			var scheduleRepository = new Mock<IScheduleRepository>();

		    scheduleRepository.Setup(x => x.Exists(It.IsAny<string>())).ReturnsAsync(() => true);
		    scheduleRepository.Setup(x => x.MakeKey(It.Is<int>(m => m == 8), It.Is<int>(y => y == 2018))).Returns(expected1);
		    scheduleRepository.Setup(x => x.MakeKey(It.Is<int>(m => m == 9), It.Is<int>(y => y == 2018))).Returns(expected2);
			scheduleRepository.Setup(x => x.Get(
				    It.IsAny<string>(),
				    It.IsAny<string>())
			    )
			    .ReturnsAsync(() => new ScheduleModel
				    {
					    ScheduledDayModels = new List<SupportDayModel>()
				    }
			    );
			var allocationManager = new AllocationSwapManager(scheduleRepository.Object);

			await allocationManager.GetSwapAllocations(Guid.NewGuid(), new DateTime(2018,8,1));

			scheduleRepository.Verify(x => x.Exists(It.Is<string>(k => k == expected1)), Times.Once);
		    scheduleRepository.Verify(x => x.Exists(It.Is<string>(k => k == expected2)), Times.Once);
		}

	    [Fact]
	    public async Task GetSwapAllocations_CallsRepositoryGet()
	    {
		    var expected1 = "2018.8";
		    var expected2 = "2018.9";
		    var scheduleRepository = new Mock<IScheduleRepository>();

		    scheduleRepository.Setup(x => x.Exists(It.IsAny<string>())).ReturnsAsync(() => true);
		    scheduleRepository.Setup(x => x.MakeKey(It.Is<int>(m => m == 8), It.Is<int>(y => y == 2018))).Returns(expected1);
		    scheduleRepository.Setup(x => x.MakeKey(It.Is<int>(m => m == 9), It.Is<int>(y => y == 2018))).Returns(expected2);
		    scheduleRepository.Setup(x => x.Get(
				    It.IsAny<string>(),
				    It.IsAny<string>())
			    )
			    .ReturnsAsync(() => new ScheduleModel
				    {
					    ScheduledDayModels = new List<SupportDayModel>()
				    }
			    );
		    var allocationManager = new AllocationSwapManager(scheduleRepository.Object);

		    await allocationManager.GetSwapAllocations(Guid.NewGuid(), new DateTime(2018, 8, 1));

		    scheduleRepository.Verify(x => x.Get(It.Is<string>(k => k == expected1), null), Times.Once);
		    scheduleRepository.Verify(x => x.Get(It.Is<string>(k => k == expected2), null), Times.Once);
	    }


		[Fact]
	    public async Task GetSwapAllocations_NoSupportDays_ReturnsNull()
	    {
		    var scheduleRepository = new Mock<IScheduleRepository>();
		    scheduleRepository.Setup(x => x.MakeKey(It.IsAny<int>(), It.IsAny<int>())).Returns("");
			scheduleRepository.Setup(x => x.Exists(It.IsAny<string>())).ReturnsAsync(() => true);
		    scheduleRepository.Setup(x => x.Get(
				    It.IsAny<string>(),
				    It.IsAny<string>())
			    )
			    .ReturnsAsync(() => new ScheduleModel
				    {
					    ScheduledDayModels = new List<SupportDayModel>()
				    }
			    );

			var allocationManager = new AllocationSwapManager(scheduleRepository.Object);
			var response = await allocationManager.GetSwapAllocations(Guid.NewGuid(), DateTime.Now);
		    Assert.Null(response);
	    }

	    [Fact]
	    public async Task GetSwapAllocations_EngineerIdNotInDayRequested_ReturnsNull()
	    {
		    var engineerId = Guid.NewGuid();

		    var scheduleRepository = new Mock<IScheduleRepository>();
		    scheduleRepository.Setup(x => x.MakeKey(It.IsAny<int>(), It.IsAny<int>())).Returns("");
			scheduleRepository.Setup(x => x.Exists(It.IsAny<string>())).ReturnsAsync(() => true);
			scheduleRepository.Setup(x => x.Get(
				    It.IsAny<string>(),
				    It.IsAny<string>())
			    )
			    .ReturnsAsync(() => new ScheduleModel
				    {
					    ScheduledDayModels = new List<SupportDayModel>
					    {
						    new SupportDayModel {AmAssignment = new EngineerModel {Id = engineerId}, CalendarDay = DateTime.Now.Date},
						    new SupportDayModel {AmAssignment = new EngineerModel {Id = new Guid()}, CalendarDay = DateTime.Now.Date.AddDays(10)}
						}
				    }
			    );

		    var allocationManager = new AllocationSwapManager(scheduleRepository.Object);
		    var response = await allocationManager.GetSwapAllocations(engineerId, DateTime.Now.Date);

			Assert.Empty(response);
	    }


	    [Fact]
	    public async Task GetSwapAllocations_NoAvailableToAssignTo_ReturnsEmpty()
	    {
			var scheduleRepository = new Mock<IScheduleRepository>();

		    scheduleRepository.Setup(x => x.Exists(It.Is<string>(k => k == "2018.8"))).ReturnsAsync(() => true);
		    scheduleRepository.Setup(x => x.MakeKey(It.IsAny<int>(), It.IsAny<int>())).Returns("");
		    scheduleRepository.Setup(x => x.MakeKey(It.Is<int>(k => k == 8), It.Is<int>(k => k == 2018))).Returns("2018.8");
			scheduleRepository.Setup(x => x.Get(
				    It.IsAny<string>(),
				    It.IsAny<string>())
			    )
			    .ReturnsAsync(() => new ScheduleModel
				    {
					    ScheduledDayModels = new List<SupportDayModel>
					    {
						    new SupportDayModel { CalendarDay = new DateTime(2018, 8, 1), AmAssignment = _eng0 },
						    new SupportDayModel { CalendarDay = new DateTime(2018, 8, 2), AmAssignment = _eng1, PmAssignment = _eng2 },
						    new SupportDayModel { CalendarDay = new DateTime(2018, 8, 3), AmAssignment = _eng0, PmAssignment = _eng2 },
						    new SupportDayModel { CalendarDay = new DateTime(2018, 8, 4), AmAssignment = _eng0, PmAssignment = _eng0 },
						    new SupportDayModel { CalendarDay = new DateTime(2018, 8, 5), AmAssignment = _eng3, PmAssignment = _eng4 },
						}
				    }
			    );
		    var allocationManager = new AllocationSwapManager(scheduleRepository.Object);

		    var response = await allocationManager.GetSwapAllocations(_eng0.Id, new DateTime(2018, 8, 1));
			Assert.Empty(response);
	    }

	    [Fact]
	    public async Task GetSwapAllocations_TwoAvailable_ReturnsTwo()
	    {
		    var scheduleRepository = new Mock<IScheduleRepository>();

		    scheduleRepository.Setup(x => x.Exists(It.Is<string>(k => k == "2018.8"))).ReturnsAsync(() => true);
		    scheduleRepository.Setup(x => x.MakeKey(It.IsAny<int>(), It.IsAny<int>())).Returns("");
		    scheduleRepository.Setup(x => x.MakeKey(It.Is<int>(k => k == 8), It.Is<int>(k => k == 2018))).Returns("2018.8");
			scheduleRepository.Setup(x => x.Get(
				    It.Is<string>(k => k == "2018.8"),
				    It.IsAny<string>())
			    )
			    .ReturnsAsync(() => new ScheduleModel
				    {
					    ScheduledDayModels = new List<SupportDayModel>
					    {
						    new SupportDayModel { CalendarDay = new DateTime(2018, 8, 1), AmAssignment = _eng0 },
						    new SupportDayModel { CalendarDay = new DateTime(2018, 8, 2), AmAssignment = _eng1, PmAssignment = _eng2 },
						    new SupportDayModel { CalendarDay = new DateTime(2018, 8, 4), AmAssignment = _eng0, PmAssignment = _eng2 },
						    new SupportDayModel { CalendarDay = new DateTime(2018, 8, 5), AmAssignment = _eng0, PmAssignment = _eng0 },
						    new SupportDayModel { CalendarDay = new DateTime(2018, 8, 6), AmAssignment = _eng3, PmAssignment = _eng4 },
						    new SupportDayModel { CalendarDay = new DateTime(2018, 8, 7), AmAssignment = _eng5, PmAssignment = _eng6 },
						}
				    }
			    );
		    var allocationManager = new AllocationSwapManager(scheduleRepository.Object);

		    var response = await allocationManager.GetSwapAllocations(_eng0.Id, new DateTime(2018, 8, 1));
			Assert.Equal(2, response.Count);
		    Assert.Equal(new DateTime(2018, 8, 7), response[0].CalendarDate);
		    Assert.Equal(new DateTime(2018, 8, 7), response[1].CalendarDate);
		    Assert.Equal(_eng5.Id, response[0].Engineer.Id);
		    Assert.Equal(_eng6.Id, response[1].Engineer.Id);
		    Assert.Equal(TimeOfDay.Am, response[0].Slot);
		    Assert.Equal(TimeOfDay.Pm, response[1].Slot);
		}

	    [Fact]
	    public async Task GetSwapAllocations_NoAvailableToAssignBack_ReturnsOne()
	    {
		    var scheduleRepository = new Mock<IScheduleRepository>();

		    scheduleRepository.Setup(x => x.Exists(It.Is<string>(k => k == "2018.8"))).ReturnsAsync(() => true);
		    scheduleRepository.Setup(x => x.MakeKey(It.IsAny<int>(), It.IsAny<int>())).Returns("");
		    scheduleRepository.Setup(x => x.MakeKey(It.Is<int>(k => k == 8), It.Is<int>(k => k == 2018))).Returns("2018.8");
		    scheduleRepository.Setup(x => x.Get(
				    It.Is<string>(k => k == "2018.8"),
				    It.IsAny<string>())
			    )
			    .ReturnsAsync(() => new ScheduleModel
				    {
					    ScheduledDayModels = new List<SupportDayModel>
					    {
						    new SupportDayModel { CalendarDay = new DateTime(2018, 8, 1), AmAssignment = _eng0, PmAssignment = _eng6},
						    new SupportDayModel { CalendarDay = new DateTime(2018, 8, 2), AmAssignment = _eng1, PmAssignment = _eng2 },
						    new SupportDayModel { CalendarDay = new DateTime(2018, 8, 4), AmAssignment = _eng0, PmAssignment = _eng2 },
						    new SupportDayModel { CalendarDay = new DateTime(2018, 8, 5), AmAssignment = _eng0, PmAssignment = _eng0 },
						    new SupportDayModel { CalendarDay = new DateTime(2018, 8, 6), AmAssignment = _eng3, PmAssignment = _eng4 },
						    new SupportDayModel { CalendarDay = new DateTime(2018, 8, 7), AmAssignment = _eng5, PmAssignment = _eng6 },
					    }
				    }
			    );
		    var allocationManager = new AllocationSwapManager(scheduleRepository.Object);

		    var response = await allocationManager.GetSwapAllocations(_eng0.Id, new DateTime(2018, 8, 1));
		    Assert.Single(response);
		    Assert.Equal(new DateTime(2018, 8, 7), response[0].CalendarDate);
		    Assert.Equal(_eng5.Id, response[0].Engineer.Id);
		    Assert.Equal(TimeOfDay.Am, response[0].Slot);
	    }
	}
}
