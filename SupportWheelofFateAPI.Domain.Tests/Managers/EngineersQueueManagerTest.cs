﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using SupportWheelofFateAPI.Domain.Managers;
using SupportWheelofFateAPI.Domain.Models;
using SupportWheelofFateAPI.Domain.Services;
using Xunit;

namespace SupportWheelofFateAPI.Domain.Tests.Managers
{
    public class EngineersQueueManagerTest
    {
	    private readonly Mock<IEngineersService> _mEngineersService;

	    private static IList<EngineerModel> OExampleEngineers
	    {
		    get
		    {
			    var examples = new List<string[]>
			    {
				    new[] {"1ef1d288-eca9-403a-bb1e-96fe21e13052", "John"},
				    new[] {"ffd468bf-1c95-4252-b723-77e8e54fa819", "Ola"},
				    new[] {"af29599c-ae62-4e25-9df3-7a34d5caa710", "Adam"},
				    new[] {"1447ca7c-39b2-464f-91eb-e55f7566cbd9", "Ada"},
				    new[] {"4a089dcb-86ab-4b5d-bf5a-fc138b2e7f24", "Konrad"},
				    new[] {"1d4bd702-d7a2-465a-86d1-af59a6be1b98", "Lily",},
				    new[] {"b5725820-add5-4649-bd82-04f64d08c77c", "Olaf",},
				    new[] {"43db7613-9e2d-4cca-b09a-3be97e3cc6fb", "Jeremi"},
				    new[] {"f35e2004-8642-40c5-9519-8e1858b8eb69", "Karina"},
				    new[] {"2fe3525b-6154-42e2-898f-f7381223403f", "Ash"},
			    };
			    return examples.Select(e => new EngineerModel { Id = new Guid(e[0]), Name = e[1] }).ToList();
		    }
	    }

		public EngineersQueueManagerTest()
	    {

		    _mEngineersService = new Mock<IEngineersService>();
		    _mEngineersService.Setup(m => m.GetEngineers()).Returns(OExampleEngineers);
		}

	    [Fact]
	    public void NextAm_ReturnsEngineer()
	    {
		    var queueManager = new EngineersQueueManager(_mEngineersService.Object);
		    var am = queueManager.NextAm();
		    Assert.NotNull(am);
		    Assert.IsAssignableFrom<EngineerModel>(am);
	    }

	    [Fact]
	    public void NextAm_ExecutedTwice_ReturnsDifferentEngineer()
	    {
		    var queueManager = new EngineersQueueManager(_mEngineersService.Object);
			
		    var am_first = queueManager.NextAm();
			var am_second = queueManager.NextAm();
		    Assert.NotEqual(am_first, am_second);
		}

	    [Fact]
	    public void NextPm_ExecutedTwice_ReturnsDifferentEngineer()
	    {
		    var queueManager = new EngineersQueueManager(_mEngineersService.Object);

		    var am_first = queueManager.NextPm();
		    var am_second = queueManager.NextPm();
		    Assert.NotEqual(am_first, am_second);
	    }

		[Fact]
	    public void NextPm_ReturnsEngineer()
	    {
		    var queueManager = new EngineersQueueManager(_mEngineersService.Object);
		    var pm = queueManager.NextPm();
		    Assert.NotNull(pm);
		    Assert.IsAssignableFrom<EngineerModel>(pm);
		}

	    [Fact]
	    public void NextAm_FullRound_ReturnsSameEngineer()
	    {
		    var queueManager = new EngineersQueueManager(_mEngineersService.Object);
		    var am_1 = queueManager.NextAm();
		    var am_n = am_1;
		    for (var i = 0; i < OExampleEngineers.Count; i++)
		    {
			    am_n = queueManager.NextAm();
		    }
			Assert.Equal(am_1, am_n);
	    }

	    [Fact]
	    public void NextPm_WaitUntilSameEngineer_FullRoundIsMade()
	    {
		    var expected = OExampleEngineers.Count;
		    var counter = 0;

		    var queueManager = new EngineersQueueManager(_mEngineersService.Object);
		    var pm_n = queueManager.NextPm();
		    var pm_first = pm_n;

			pm_n = queueManager.NextPm();
		    counter++;
			while (pm_n != pm_first)
		    {
			    pm_n = queueManager.NextPm();
			    counter++;
		    }
		    Assert.Equal(expected, counter);
	    }

		[Fact]
        public void NextAm_ReturnsExpectedEngineer()
        {
	        var erngineersService = new Mock<IEngineersService>();
	        var id = Guid.NewGuid();
	        erngineersService.Setup(m => m.GetEngineers())
		        .Returns(new List<EngineerModel> {new EngineerModel {Id = id}, new EngineerModel {Id = id}});
	        var queueManager = new EngineersQueueManager(erngineersService.Object);
			var am = queueManager.NextAm();
			Assert.Equal(id, am.Id);
		}

	    [Fact]
	    public void NextPm_ReturnsExpectedEngineer()
	    {
		    var erngineersService = new Mock<IEngineersService>();
		    var id = Guid.NewGuid();
		    erngineersService.Setup(m => m.GetEngineers())
			    .Returns(new List<EngineerModel> { new EngineerModel { Id = id }, new EngineerModel { Id = id } });
		    var queueManager = new EngineersQueueManager(erngineersService.Object);
		    var pm = queueManager.NextPm();
		    Assert.Equal(id, pm.Id);
		}
	}
}
