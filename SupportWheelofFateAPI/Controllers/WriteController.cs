﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SupportWheelofFateAPI.Domain.EventRouting;
using SupportWheelofFateAPI.Domain.Events;
using SupportWheelofFateAPI.Domain.EventStore;
using SupportWheelofFateAPI.Domain.Managers;
using SupportWheelofFateAPI.Domain.Models;
using SupportWheelofFateAPI.Domain.Repositories;
using SupportWheelofFateAPI.Helpers;
using SupportWheelofFateAPI.Models;

namespace SupportWheelofFateAPI.Controllers
{
	[ApiVersion("1.0")]
	[Produces("application/json")]
    [Route("schedule")]
    public class WriteController : Controller
	{
	    private readonly IScheduleManager _scheduleManager;
	    private readonly IScheduleRepository _scheduleRepository;
	    private readonly IEventStore _eventStore;
	    private readonly IEventPublisher _eventPublisher;
		private readonly IAllocationSwapManager _swapManager;

		public WriteController(IScheduleManager scheduleManager, IScheduleRepository scheduleRepository, IEventStore eventStore, IEventPublisher eventPublisher, IAllocationSwapManager _swapManager)
	    {
		    _eventPublisher = eventPublisher;
		    this._swapManager = _swapManager;
		    _scheduleManager = scheduleManager;
		    _scheduleRepository = scheduleRepository;
		    _eventStore = eventStore;
	    }

		/// <summary>
		/// Rebuilds the view model from events.
		/// </summary>
		/// <param name="scheduleKeys">List of keys of the schedules to refresh.</param>
		/// <returns>Returns number of events found for that month.</returns>
		[HttpPost]
	    [Route("refresh")]
		public async Task<ActionResult> RebuildViewModel([FromBody] string[] scheduleKeys)
	    {
		    try
		    {
				var updater = new Updater(_eventPublisher, _eventStore, _scheduleRepository);
			    var result = await updater.RefreshReadModel(scheduleKeys);

			    return Ok(result);
		    }
		    catch (Exception e)
		    {
			    return BadRequest(e);
		    }
	    }

		/// <summary>
		/// Populates monthly schedule
		/// </summary>
		/// <param name="request"></param>
		/// <returns></returns>
		[HttpPost]
	    [Route("month")]
	    public async Task<ActionResult> PopulateSchedule([FromBody] PopulateScheduleRequestModel request)
		{
			if (request.year < DateTime.Now.Year || request.year > DateTime.Now.Year + 1)
				return BadRequest("Needs to be this year or the next.");

			if (request.month > 12 || request.month <= 0)
				return BadRequest("Incorrect month.");

			if (new DateTime(request.year, request.month, DateTime.Now.Day) < DateTime.Now.Date)
				return BadRequest("Can populate schedule only for the future.");

			var currentKey = _scheduleRepository.MakeKey(request.month, request.year);

			if (await _scheduleRepository.Exists(currentKey))
				return Forbid("Schedule with this key already exists.");

			var previousMonthDay = new DateTime(request.year, request.month, 1).AddMonths(-1);
			var previousKey = _scheduleRepository.MakeKey(previousMonthDay.Month, previousMonthDay.Year);

			SupportDayModel lastSupportDay = null;

			if (await _scheduleRepository.Exists(previousKey))
			{
				var previouScheduleModel = await _scheduleRepository.Get(previousKey);
				lastSupportDay = previouScheduleModel?.ScheduledDayModels?.OrderBy(s => s.CalendarDay)?.LastOrDefault();
			}

			var lastDayEngineerIds = new[] { lastSupportDay?.AmAssignment?.Id, lastSupportDay?.PmAssignment?.Id };

			var events = _scheduleManager.GetBookedEvents(lastDayEngineerIds, request.year, request.month);

			foreach (var @event in events)
			{
				await _eventStore.Save(@event);
			}

			return Ok();
		}

		/// <summary>
		/// Swap two assignments
		/// </summary>
		/// <param name="swapRequestModel"></param>
		/// <returns></returns>
		[HttpPost]
		[Route("swap")]
		public async Task<ActionResult> SetSwap([FromBody] SwapRequestModel swapRequestModel)
		{
			if (swapRequestModel == null || swapRequestModel.engineerFromId == Guid.Empty || swapRequestModel.dateFrom.Date <= DateTime.UtcNow.Date)
				return BadRequest("Invalid request");

			try
			{
				if (!await IsValidateRequest(swapRequestModel))
					return BadRequest("Allocation requested not available");

				var fromKey = _scheduleRepository.MakeKey(swapRequestModel.dateFrom.Month, swapRequestModel.dateFrom.Year);
				var toKey = _scheduleRepository.MakeKey(swapRequestModel.dateFrom.Month, swapRequestModel.dateFrom.Year);

				// Assign requesting engineer to a destination date
				var toBookedEvent = new BookedEvent
				{
					Id = Guid.NewGuid(),
					UTCTimestamp = DateTime.UtcNow,
					ScheduleId = toKey,
					Date = swapRequestModel.dateTo,
					TimeOfDay = swapRequestModel.destinationTimeSlot,
					EngineerId = swapRequestModel.engineerFromId
				};
				await _eventStore.Save(toBookedEvent);

				// Assign engineer at destination date to a requested from date
				var fromBookedEvent = new BookedEvent
				{
					Id = Guid.NewGuid(),
					UTCTimestamp = DateTime.UtcNow,
					ScheduleId = fromKey,
					Date = swapRequestModel.dateFrom,
					TimeOfDay = swapRequestModel.sourceTimeSlot,
					EngineerId = swapRequestModel.engineerToId
				};
				await _eventStore.Save(fromBookedEvent);
				return Ok();
			}
			catch (Exception e)
			{
				return BadRequest(e.Message);
			}
		}

		private async Task<bool> IsValidateRequest(SwapRequestModel swapRequestModel)
		{
			var allocations = await _swapManager.GetSwapAllocations(swapRequestModel.engineerFromId, swapRequestModel.dateFrom);
			return allocations.Exists(a => a.CalendarDate == swapRequestModel.dateTo && a.Engineer.Id == swapRequestModel.engineerToId);
		}
	}
}