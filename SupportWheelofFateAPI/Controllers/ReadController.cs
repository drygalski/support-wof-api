﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.KeyVault.Models;
using Newtonsoft.Json.Linq;
using SupportWheelofFateAPI.Domain;
using SupportWheelofFateAPI.Domain.Managers;
using SupportWheelofFateAPI.Domain.Models;
using SupportWheelofFateAPI.Domain.Repositories;
using SupportWheelofFateAPI.Domain.Services;

namespace SupportWheelofFateAPI.Controllers
{
	[ApiVersion("1.0")]
	[Route("schedule")]
	[Produces("application/json")]
	public class ReadController : Controller
    {
	    private readonly IScheduleRepository _scheduleRepository;
	    private readonly IEngineersService _engineersService;
	    private readonly IAllocationSwapManager _swapManager;

	    public ReadController(IScheduleRepository scheduleRepository, IEngineersService engineersService, IAllocationSwapManager swapManager)
	    {
		    _scheduleRepository = scheduleRepository;
		    _engineersService = engineersService;
		    _swapManager = swapManager;
	    }

		/// <summary>
		/// Gets monthly schedule for a given callendar month.
		/// </summary>
		/// <param name="year">Long year number</param>
		/// <param name="month">Calendar month number.</param>
		/// <returns>Schedule Model</returns>
		[HttpGet]
		[Route("month/{year}/{month}")]
		public async Task<ActionResult> GetMonth(int year, int month)
	    {
		    try
		    {
			    if (month > 12 || month <= 0 || year <= 0)
				    return BadRequest();

			    var key = _scheduleRepository.MakeKey(month, year);

			    if (!await _scheduleRepository.Exists(key))
				    return NotFound(key);

			    var schedule = await _scheduleRepository.Get(key);

			    return Ok(schedule);
			}
		    catch (Exception e)
		    {
			    return BadRequest(e.Message);
		    }
		}

		/// <summary>
		/// Gets a list of all available engineers.
		/// </summary>
		/// <returns></returns>
	    [HttpGet]
	    [Route("engineer/list")]
	    public ActionResult GetEngineers()
	    {
		    try
		    {
			    var engineers = _engineersService.GetEngineers();
			    return Ok(engineers);
			}
		    catch (Exception e)
		    {
			    return BadRequest(e.Message);
		    }
		}

		/// <summary>
		/// Gets a list of potential swap allocations
		/// </summary>
		/// <param name="engineerFrom">Requesting engineer id</param>
		/// <param name="dateFrom">Requested day</param>
		/// <returns></returns>
	    [HttpGet]
	    [Route("swap/{engineerFrom}/{dateFrom}/list")]
	    public async Task<ActionResult> GetSwapAllowedDays(Guid engineerFrom, DateTime dateFrom)
	    {
		    try
		    {
			    if (dateFrom.Date <= DateTime.Now.Date) return BadRequest("Can only swap future asignments.");

			    var allocations = await _swapManager.GetSwapAllocations(engineerFrom, dateFrom);
			    return Ok(allocations);
		    }
		    catch (Exception e)
		    {
			    return BadRequest(e.Message);
		    }
	    }
    }
}
