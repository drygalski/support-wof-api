﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace SupportWheelofFateAPI.Middleware
{
    public class HealthCheckMiddleware
    {
	    private readonly RequestDelegate _next;

	    public HealthCheckMiddleware(RequestDelegate next)
	    {
		    _next = next;
	    }

	    public Task Invoke(HttpContext context)
	    {
		    return context.Request.Path.Equals("/check") ? context.Response.WriteAsync($"ok") : _next(context);
	    }
	}
}
