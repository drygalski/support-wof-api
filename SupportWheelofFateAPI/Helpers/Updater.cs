﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SupportWheelofFateAPI.Domain.EventRouting;
using SupportWheelofFateAPI.Domain.Events;
using SupportWheelofFateAPI.Domain.EventStore;
using SupportWheelofFateAPI.Domain.Repositories;

namespace SupportWheelofFateAPI.Helpers
{
	public class Updater
	{
		private readonly IEventPublisher _eventPublisher;
		private readonly IEventStore _eventStore;
		private readonly IScheduleRepository _scheduleRepository;

		public Updater(IEventPublisher eventPublisher, IEventStore eventStore, IScheduleRepository scheduleRepository)
		{
			_eventPublisher = eventPublisher;
			_eventStore = eventStore;
			_scheduleRepository = scheduleRepository;
		}

		public async Task<int> RefreshReadModel(string[] scheduleKeys, CancellationToken cancellationToken = new CancellationToken())
		{
			var counter = 0;
			foreach (var scheduleKey in scheduleKeys ?? new string[0])
			{
				if (await _scheduleRepository.Exists(scheduleKey))
				{
					await _scheduleRepository.DeleteAsync(scheduleKey, null);
				}
				var events = await _eventStore.Get(scheduleKey);

				foreach (var @event in events ?? new IEvent[0])
				{
					await _eventPublisher.Publish(@event, cancellationToken);
				}
				counter += events?.Count() ?? 0;
			}
			return counter;
		}
	}
}