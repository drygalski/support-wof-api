﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace SupportWheelofFateAPI
{
	/// <summary>
	/// Program
	/// </summary>
	public class Program
    {
		/// <summary>
		/// Main method
		/// </summary>
		/// <param name="args">args</param>
		public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
