using System.Collections.Generic;
using SupportWheelofFateAPI.Domain.Models;

namespace SupportWheelofFateAPI.Domain.Services
{
	public interface IEngineersService
	{
		IList<EngineerModel> GetEngineers();
	}
}