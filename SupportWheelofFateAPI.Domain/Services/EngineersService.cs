using System;
using System.Collections.Generic;
using System.Linq;
using SupportWheelofFateAPI.Domain.Managers;
using SupportWheelofFateAPI.Domain.Models;

namespace SupportWheelofFateAPI.Domain.Services
{
	public class EngineersService : IEngineersService
	{
		public IList<EngineerModel> GetEngineers()
		{
			var examples = new List<string[]>
				{
					new[] {"1ef1d288-eca9-403a-bb1e-96fe21e13052", "John"},
					new[] {"ffd468bf-1c95-4252-b723-77e8e54fa819", "Ola"},
					new[] {"af29599c-ae62-4e25-9df3-7a34d5caa710", "Adam"},
					new[] {"1447ca7c-39b2-464f-91eb-e55f7566cbd9", "Ada"},
					new[] {"4a089dcb-86ab-4b5d-bf5a-fc138b2e7f24", "Konrad"},
					new[] {"1d4bd702-d7a2-465a-86d1-af59a6be1b98", "Lily",},
					new[] {"b5725820-add5-4649-bd82-04f64d08c77c", "Olaf",},
					new[] {"43db7613-9e2d-4cca-b09a-3be97e3cc6fb", "Jeremi"},
					new[] {"f35e2004-8642-40c5-9519-8e1858b8eb69", "Karina"},
					new[] {"2fe3525b-6154-42e2-898f-f7381223403f", "Ash"},
				};
			return examples.Select(e => new EngineerModel { Id = new Guid(e[0]), Name = e[1] }).ToList();
		}
	}
}
