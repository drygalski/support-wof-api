﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SupportWheelofFateAPI.Domain.Events;


namespace SupportWheelofFateAPI.Domain.EventRouting
{
	/// <summary>
	/// This code has been mostly copied from:
	/// https://github.com/gautema/CQRSlite/blob/master/Framework/CQRSlite/Routing/Router.cs
	/// </summary>
	public class Router : IHandlerRegistrar, IEventPublisher
	{
		private readonly Dictionary<Type, List<Func<IEvent, CancellationToken, Task>>> _routes = new Dictionary<Type, List<Func<IEvent, CancellationToken, Task>>>();

		public void RegisterHandler<T>(Func<T, CancellationToken, Task> handler) where T : class, IEvent
		{
			if (!_routes.TryGetValue(typeof(T), out var handlers))
			{
				handlers = new List<Func<IEvent, CancellationToken, Task>>();
				_routes.Add(typeof(T), handlers);
			}
			handlers.Add((message, token) => handler((T)message, token));
		}

		public Task Publish<T>(T @event, CancellationToken cancellationToken = default(CancellationToken)) where T : class, IEvent
		{
			if (!_routes.TryGetValue(@event.GetType(), out var handlers))
				return Task.FromResult(0);

			return Task.WhenAll(handlers.Select(handler => handler(@event, cancellationToken)));
		}
	}
}