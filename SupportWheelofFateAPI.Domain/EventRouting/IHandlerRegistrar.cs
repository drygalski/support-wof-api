﻿using System;
using System.Threading;
using System.Threading.Tasks;
using SupportWheelofFateAPI.Domain.Events;

namespace SupportWheelofFateAPI.Domain.EventRouting
{
	public interface IHandlerRegistrar
    {
	    void RegisterHandler<T>(Func<T, CancellationToken, Task> handler) where T : class, IEvent;
	}
}
