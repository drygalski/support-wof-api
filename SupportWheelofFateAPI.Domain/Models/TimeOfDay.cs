﻿namespace SupportWheelofFateAPI.Domain.Models
{
	public enum TimeOfDay
	{
		Am = 0,
		Pm = 1
	}
}