using System.Collections.Generic;

namespace SupportWheelofFateAPI.Domain.Models
{
	public class ScheduleModel
	{
		public string Id { get; set; }
		public List<SupportDayModel> ScheduledDayModels { get; set; }
	}
}
