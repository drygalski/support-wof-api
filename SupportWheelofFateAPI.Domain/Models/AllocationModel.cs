﻿using System;

namespace SupportWheelofFateAPI.Domain.Models
{
	public class AllocationModel
	{
		public DateTime CalendarDate { get; set; }
		public EngineerModel Engineer { get; set; }
		public TimeOfDay Slot { get; set; }
	}
}