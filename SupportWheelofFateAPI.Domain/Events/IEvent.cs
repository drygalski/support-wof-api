﻿using System;

namespace SupportWheelofFateAPI.Domain.Events
{ 
	public interface IEvent
	{
		Guid Id { get; set; }

		string Type { get; set; }

		DateTime UTCTimestamp { get; set; }
	}
}