﻿using System;
using SupportWheelofFateAPI.Domain.Models;


namespace SupportWheelofFateAPI.Domain.Events
{
    public class BaseEvent: IEvent
    {
	    public Guid Id { get; set; }
	    public Guid EngineerId { get; set; }
		public string ScheduleId { get; set; }
		public string Type { get; set; }
	    public DateTime UTCTimestamp { get; set; }
		public TimeOfDay TimeOfDay { get; set; }
	    public DateTime Date { get; set; }
	}
}
