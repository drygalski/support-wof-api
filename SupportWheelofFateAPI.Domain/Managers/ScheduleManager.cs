﻿using System;
using System.Collections.Generic;
using System.Linq;
using SupportWheelofFateAPI.Domain.Events;
using SupportWheelofFateAPI.Domain.Models;
using SupportWheelofFateAPI.Domain.Services;
using SupportWheelofFateAPI.Domain.Validators;

namespace SupportWheelofFateAPI.Domain.Managers
{
	public class ScheduleManager: IScheduleManager
    {
	    private readonly IEngineersService _engineersService;
	    private readonly IScheduleValidator _scheduleValidator;
	    private readonly IEngineersQueueManager _queueManager;
	    private static IEnumerable<DayOfWeek> WeekendDays => new[] { DayOfWeek.Saturday, DayOfWeek.Sunday };

		public ScheduleManager(IEngineersService engineersService, IScheduleValidator scheduleValidator, IEngineersQueueManager queueManager)
		{
			_engineersService = engineersService;
			_scheduleValidator = scheduleValidator;
			_queueManager = queueManager;
		}

	    public ScheduleModel GenerateMonthPlan(Guid?[] lastDayEngineerIds, int thisYear, int thisMonth)
	    {
		    if (thisYear == 0 || thisMonth == 0)
			    return null;

		    var newMonthPlan = new List<SupportDayModel>();
		    var firstDay = new DateTime(thisYear, thisMonth, 1, 0, 0, 0, DateTimeKind.Utc);
		    var lastDay = firstDay.AddMonths(1).AddDays(-1);
		    var nextDay = firstDay;

			while (nextDay <= lastDay)
		    {
			    if (WeekendDays.Contains(nextDay.DayOfWeek))
			    {
				    nextDay = nextDay.AddDays(1);
					continue;
			    }
			    
			    var day = new SupportDayModel
			    {
				    CalendarDay = nextDay,
				    AmAssignment = _queueManager.NextAm(),
				    PmAssignment = _queueManager.NextPm()
			    };
			    newMonthPlan.Add(day);
				nextDay = nextDay.AddDays(1);
		    }

		    var isValid = !_scheduleValidator.ValidateSchedule(newMonthPlan, _engineersService.GetEngineers());

			return !isValid ? new ScheduleModel {Id = $"{thisYear}.{thisMonth}", ScheduledDayModels = newMonthPlan} : null;
	    }
	    public IEnumerable<BookedEvent> GetBookedEvents(Guid?[] lastDayEngineerIds, int thisYear, int thisMonth)
	    {
		    var schedule = GenerateMonthPlan(lastDayEngineerIds, thisYear, thisMonth);

		    var events = new List<BookedEvent>();
		    foreach (var day in schedule.ScheduledDayModels)
		    {
				if (day.AmAssignment != null)
				{
					var @event = new BookedEvent
					{
						Id = Guid.NewGuid(),
						Date = day.CalendarDay,
						EngineerId = day.AmAssignment.Id,
						TimeOfDay = TimeOfDay.Am,
						UTCTimestamp = DateTime.UtcNow,
						ScheduleId = $"{day.CalendarDay.Year}.{day.CalendarDay.Month}"
					};
					events.Add(@event);
			    }

			    if (day.PmAssignment != null)
			    {
				    var @event = new BookedEvent
				    {
					    Id = Guid.NewGuid(),
					    Date = day.CalendarDay,
					    EngineerId = day.PmAssignment.Id,
					    TimeOfDay = TimeOfDay.Pm,
					    UTCTimestamp = DateTime.UtcNow,
					    ScheduleId = $"{day.CalendarDay.Year}.{day.CalendarDay.Month}"
				    };
				    events.Add(@event);
			    }
			}
		    return events;

	    }
    }
}
