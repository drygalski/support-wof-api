using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SupportWheelofFateAPI.Domain.Models;

namespace SupportWheelofFateAPI.Domain.Managers
{
	public interface IAllocationSwapManager
	{
		Task<List<AllocationModel>> GetSwapAllocations(Guid engineerId, DateTime requestDay);
	}
}