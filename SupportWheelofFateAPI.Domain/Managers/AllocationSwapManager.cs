using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SupportWheelofFateAPI.Domain.Models;
using SupportWheelofFateAPI.Domain.Repositories;

namespace SupportWheelofFateAPI.Domain.Managers
{
	public class AllocationSwapManager : IAllocationSwapManager
	{
		private readonly IScheduleRepository _scheduleRepository;
		private List<SupportDayModel> _supportDayModels;

		public AllocationSwapManager(IScheduleRepository scheduleRepository)
		{
			_scheduleRepository = scheduleRepository;
		}

		public async Task<List<AllocationModel>> GetSwapAllocations(Guid engineerId, DateTime requestDay)
		{
			await GetSchedules(requestDay);

			if (!_supportDayModels.Any()) return null;

			// Days to which engineer can assign himself to
			var futureDays = _supportDayModels.Where(d => d.CalendarDay > DateTime.UtcNow.Date);
			var filteredDesitnationDays = futureDays.Where(d => IsDayAvaiailable(engineerId, d.CalendarDay));

			// Allocations from which engineers can be assign back to the day requested
			var requestDayNeigbourhodEngineerIds = GetDayNeighbourhood(requestDay)?.SelectMany(d => new[] { d.AmAssignment?.Id, d.PmAssignment?.Id }).Where(i => i.HasValue).ToArray();

			if (!requestDayNeigbourhodEngineerIds.Contains(engineerId)) return null;

			var allocations = new List<AllocationModel>();

			foreach (var day  in filteredDesitnationDays)
			{
				if (day.AmAssignment != null && !requestDayNeigbourhodEngineerIds.Contains(day.AmAssignment.Id))
				{
					allocations.Add(new AllocationModel
					{
						CalendarDate = day.CalendarDay,
						Engineer = day.AmAssignment,
						Slot = TimeOfDay.Am
					});
				}

				if (day.PmAssignment != null && !requestDayNeigbourhodEngineerIds.Contains(day.PmAssignment.Id))
				{
					allocations.Add(new AllocationModel
					{
						CalendarDate = day.CalendarDay,
						Engineer = day.PmAssignment,
						Slot = TimeOfDay.Pm
					});
				}
			} 
			return allocations;
		}

		private async Task GetSchedules(DateTime requestDay)
		{
			_supportDayModels = new List<SupportDayModel>();

			var thisMonthKey = _scheduleRepository.MakeKey(requestDay.Month, requestDay.Year);
			var nextMonthKey = _scheduleRepository.MakeKey(requestDay.AddMonths(1).Month, requestDay.AddMonths(1).Year);


			if (await _scheduleRepository.Exists(thisMonthKey))
			{
				var thisMonthSchedule = await _scheduleRepository.Get(thisMonthKey);
				_supportDayModels.AddRange(thisMonthSchedule?.ScheduledDayModels);
			}

			if (await _scheduleRepository.Exists(nextMonthKey))
			{
				var nextMonthSchedule = await _scheduleRepository.Get(nextMonthKey);
				_supportDayModels.AddRange(nextMonthSchedule?.ScheduledDayModels);
			}
		}

		private bool IsDayAvaiailable(Guid engineerId, DateTime requestDay)
		{
			var neigbourhod = GetDayNeighbourhood(requestDay);
			return !neigbourhod.Any(n => n.AmAssignment.Id == engineerId || n.PmAssignment.Id == engineerId);
		}

		private IEnumerable<SupportDayModel> GetDayNeighbourhood(DateTime requestDay)
		{
			var response = new List<SupportDayModel>();
			var requestSupportDay = _supportDayModels.FirstOrDefault(m => m?.CalendarDay.Date == requestDay.Date);
			var requestDayIndex = _supportDayModels.IndexOf(requestSupportDay);

			if (requestDayIndex > 0)
			{
				var previousDay = _supportDayModels.ElementAtOrDefault(requestDayIndex - 1);
				if (previousDay != null)
					response.Add(previousDay);
			}

			if(requestSupportDay != null)
				response.Add(requestSupportDay);

			if (requestDayIndex + 1 >= _supportDayModels.Count) return response;

			var nextDay = _supportDayModels.ElementAtOrDefault(requestDayIndex + 1);

			if (nextDay != null)
				response.Add(nextDay);

			return response;
		}
	}
}