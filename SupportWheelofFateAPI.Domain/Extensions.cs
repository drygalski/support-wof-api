﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace SupportWheelofFateAPI.Domain
{
	public static class Extensions
	{
		public static T NextInRound<T>(this Queue<T> queue)
		{
			var element = queue.Dequeue();
			queue.Enqueue(element);
			return element;
		}

		public static int GetIso8601WeekOfYear(DateTime date)
		{
			var day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(date);
			if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
			{
				date = date.AddDays(3);
			}
			return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(date, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
		}

		public static void Shuffle<T>(this IList<T> list)
		{
			var random = new Random();
			var n = list.Count;

			while (n > 1)
			{
				n--;
				var key = random.Next(n + 1);
				T value = list[key];
				list[key] = list[n];
				list[n] = value;
			}
		}
	}
}
