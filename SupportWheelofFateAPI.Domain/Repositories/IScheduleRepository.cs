﻿using System;
using System.Threading.Tasks;
using SupportWheelofFateAPI.Domain.Models;

namespace SupportWheelofFateAPI.Domain.Repositories
{
	public interface IScheduleRepository
	{
		Task<ScheduleModel> Get(string scheduleKey, string lockToken = null);

		Task Save(string scheduleKey, ScheduleModel model, string lockToken = null);

		Task<bool> Exists(string scheduleKey);

		Task<bool> DeleteAsync(string key, string token);

		string MakeKey(int month, int year);
	}
}