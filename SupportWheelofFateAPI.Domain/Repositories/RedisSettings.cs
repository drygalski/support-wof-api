﻿using System;
using StackExchange.Redis;

namespace SupportWheelofFateAPI.Domain.Repositories
{
	public class RedisSettings
	{
		public string ConnectionString { get; set; }
		public IConnectionMultiplexer Multiplexer { get; set; }
		public String Environment { get; set; }
	}
}