﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using Newtonsoft.Json;
using SupportWheelofFateAPI.Domain.Events;
using SupportWheelofFateAPI.Domain.EventRouting;


namespace SupportWheelofFateAPI.Domain.EventStore
{
	public interface IEventStore
	{
		Task<IEnumerable<IEvent>> Get(string scheduleMonthId);
		Task Save(IEnumerable<IEvent> evnt, CancellationToken cancellationToken = new CancellationToken());
		Task Save(IEvent evnt, CancellationToken cancellationToken = new CancellationToken());
	}

	public class EventStore : IEventStore
	{
		private readonly IMongoDatabase _mongoDatabase;
		private readonly IEventPublisher _publisher;

		public EventStore(Settings settings, IEventPublisher publisher)
		{
			_publisher = publisher;
			MongoDefaults.GuidRepresentation = MongoDB.Bson.GuidRepresentation.Standard;

			var client = new MongoClient(settings.MongoConnectionString);
			this._mongoDatabase = client.GetDatabase(settings.MongoDatabase);
		}

		private IMongoCollection<IEvent> GetEventsDTO()
		{
			return _mongoDatabase.GetCollection<IEvent>("Events");
		}

		public async Task Save(IEnumerable<IEvent> events, CancellationToken cancellationToken = new CancellationToken())
		{
			await GetEventsDTO().InsertManyAsync(events, cancellationToken: cancellationToken);
			await Task.WhenAll(events.Select(e => _publisher.Publish(e, cancellationToken)));
		}

		public async Task Save(IEvent evnt, CancellationToken cancellationToken = new CancellationToken())
		{
			await GetEventsDTO().InsertOneAsync(evnt, null, cancellationToken);
			await _publisher.Publish(evnt, cancellationToken);
		}

		public async Task<IEnumerable<IEvent>> Get(string ScheduleId)
		{
			var filter = Builders<IEvent>.Filter.Eq("ScheduleId", ScheduleId);
			return await GetEventsDTO().Find(filter).ToListAsync();
		}

		public static void ConfigureMongoBindings()
		{
			var listOfEventTypes = (from domainAssembly in AppDomain.CurrentDomain.GetAssemblies()
				from assemblyType in domainAssembly.GetTypes()
				where assemblyType.IsSubclassOf(typeof(BaseEvent))
				select assemblyType).ToArray();

			foreach (var type in listOfEventTypes)
				BsonClassMap.LookupClassMap(type);
		}

		public class Settings
		{
			public string MongoConnectionString { get; set; }

			public string MongoDatabase { get; set; }

		}
	}
}