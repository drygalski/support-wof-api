﻿using System.Collections.Generic;
using System.Linq;
using SupportWheelofFateAPI.Domain.Models;

namespace SupportWheelofFateAPI.Domain.Validators
{
	public interface IScheduleValidator
	{
		bool ValidateSchedule(IList<SupportDayModel> supportDayModels, IList<EngineerModel> engineerModels);
	}

	public class ScheduleValidator : IScheduleValidator
	{
	    public bool ValidateSchedule(IList<SupportDayModel> supportDayModels, IList<EngineerModel> engineerModels)
	    {
		    const int daysOfWeek = 5;

		    if (supportDayModels == null || !supportDayModels.Any())
			    return false;

			// All days have to be booked
			if (supportDayModels.Any(m => !m.IsBooked))
			    return false;

		    // One engineer can't cover am and pm of the same day
			if (supportDayModels.Any(m => m.AmAssignment.Id == m.PmAssignment.Id))
			    return false;

			// One engineer can't cover two days in a row
		    var previousDay = supportDayModels.First();

			foreach (var supportDayModel in supportDayModels.Skip(1))
			{
				var previousDayEngieers = new[] {previousDay.AmAssignment.Id, previousDay.PmAssignment.Id};
				if (previousDayEngieers.Contains(supportDayModel.AmAssignment.Id) ||
				    previousDayEngieers.Contains(supportDayModel.PmAssignment.Id))
					return false;
				previousDay = supportDayModel;
			}

			// Each engineer has to cover one day worth of work in each 2 weeks
		    if (supportDayModels.Count() >= daysOfWeek * 2)
		    {
			    foreach (var engineerModel in engineerModels)
			    {
				    var amCount = supportDayModels.Count(d => d.AmAssignment.Id == engineerModel.Id);
				    var pmCount = supportDayModels.Count(d => d.PmAssignment.Id == engineerModel.Id);
					if (amCount + pmCount < 2)
					    return false;
			    }
			}
			return true;
	    }
    }
}
